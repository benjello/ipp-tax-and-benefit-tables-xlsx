Marche à suivre pour modifier les barèmes pour les contributeurs issus de l'IPP
===============================================================================

1. Rassembler les informations concernant les barèmes à modifier. Un calendrier indicatif est [disponible] (https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/blob/master/calendrier.md).

2. Ouvrir une console [shell] 
    (https://git.framasoft.org/ipp/ipp-survival-gitbook/blob/master/shell.md) 
    et se placer dans le répertoire contenant les barèmes (ne pas hésiter à 
    utiliser la touche <kbd>Tab</kbd> pour faire apparaître les noms des sous-dossiers.)

    ````shell
    cd "Z:\3-Legislation\Baremes IPP"
    ````
   L'invite de la console nous permet de vérifier que l'on effectue bien des modifications
   sur la branche `master` en local (i.e. sur notre ordinateur).
   
    ````bash
     /z/3-Legislation/Baremes IPP (master)
    ````
3. Récupérer les informations sur l'ensemble des dépôts distants, i.e. récupérer les modifications validées effectuées par les autres utilisateurs.

    ````shell
    git fetch --all
    ````
4. Vérifier si la branche locale (i.e. sur notre ordinateur) est en retard par rapport à la branche du dépôt IPP sur Gitlab (appelée ipp/master) en éxecutant:

    ````shell
    gitk --all
    ````
5. Afin de se synchroniser avec la branche du [dépôt de l'IPP hébergé par le serveur gitlab hébergé par framasoft] 
   (https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/), on importe les modifications propagées par les autres utilisateurs en éxecutant:    
    
    ````shell
    git rebase ipp/master
    ````    
6. Vérifier l'état du dépôt en exécutant: 
   
    ````shell
    git status 
    ````
    Cela nous permet de voir si des fichiers suivis ("tracked files") ont été modifiés sans avoir été commités.
7. Si les fichiers à modifier sont inchangés allez au point directement au point 8.
   Si un fichier à modifier est altéré depuis le dernier "commit", commiter les modifications en éxecutant:
    
    ````shell
    git commit
    ````

8. Modifier son fichier excel, enregistrer les modifications, puis fermer le document.

9. Puis en utilisant le shell, enregistrer ses modifications dans le système de version local
   en prenant soin d'effectuer les deux étapes suivantes:
  * n'ajouter que le/les fichier(s) modifiés à l'index en utilisant: 

    ````shell
    git add monfichier.xlsx
    ````
    ou en utilisant l'interface accessible en exécutant `git-gui`

    ````shell
    git gui
    ````
  * puis commiter les modifications en ajoutant un message de "commit" [compréhensible par tous] 
    (http://chris.beams.io/posts/git-commit/) 
    indiquant les modifications effectuées en exécutant
    
    ````shell
    git commit
    ````
    S'il y a lieu, ne pas oublier 
    d'indiquer le ticket ("issue") qui est ainsi fermé en ajoutant dans le message
    de commit le texte ci-dessous:
    
    ````txt
    Fais plein de modifications très utiles
    Closes #NumeroDuTicket`
    ````

10. Enfin, propager les modifications au [dépot gitlab hébergé par framasoft] 
   (https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/)
   en exécutant
    
   ````shell
   git push ipp master
   ````
   ou en utilisant l'interface accessible en exécutant `git-gui`
   ````shell
   git gui
   ````

11. Pour bien vérifier que ma version en local est parfaitement synchronisée avec la version à jour sur Gitlab, faire une ultime vérification en éxécutant à nouveau:
    
    ````shell
    gitk --all
    ````
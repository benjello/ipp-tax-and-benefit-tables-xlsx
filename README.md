# ipp-tax-and-benefit-tables-xlsx

Original IPP's tax and benefit tables in XLSX format.

IPP = [Institut des politiques publiques](http://www.ipp.eu/en/)

Original tax and benefit tables:
- English: http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables/
- French: http://www.ipp.eu/fr/outils/baremes-ipp/

## Data License

Licence ouverte / Open Licence <http://www.etalab.gouv.fr/licence-ouverte-open-licence>

## Contribute to IPP's tax and benefit tables

* [Contributions are welcomed] (Contributing.md).
* [Contribution guide for IPP members] (IPP_contributors.md).